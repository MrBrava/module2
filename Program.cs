using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            Console.WriteLine(program.GetMultipliedNumbers("123,456","1000"));
            Console.ReadKey();
        }

        public int GetTotalTax(int companiesNum, int procent, int revenue)
        {
            return (int)(companiesNum * revenue * procent / 100);
        }

        public string GetCongratulation(int age)
        {
            if (age % 2 == 0 && age >= 18)
                return ("Поздравляю с совершеннолетием!");
            else if (age % 2 == 1 && age > 12 && age < 18)
                return ("Поздравляю с переходом в старшую школу!");
            else
                return ($"Поздравляю с {age}-летием!");
        }

        public double GetMultipliedNumbers(string numF, string numS)
        {
            double first = 0;
            double second = 0;
            if (double.TryParse(numF, out first) && double.TryParse(numS, out second))
            {
                return first * second;
            }
            else if (DoubleParse(numF, out first) && double.TryParse(numS, out second))
            {
                return first * second;
            }
            else if (double.TryParse(numF, out first) && DoubleParse(numS, out second))
            {
                return first * second;
            }
            else if (DoubleParse(numF, out first) && DoubleParse(numS, out second))
            {
                return first * second;
            }
            else
            {
                Console.WriteLine("При вводе данных был введен не допустимый символ");
                return double.MinValue;
            }
        }

        public double GetFigureValues(FigureEnum figure, ParameterEnum parameter, Dimensions dimen)
        {
            double perimetr = 0;
            double squere = 0;
            if (figure == FigureEnum.Circle)
            {
                if (dimen.Radius > 0)
                {
                    perimetr = 2 * Math.PI * dimen.Radius;
                    squere = Math.PI * dimen.Radius * dimen.Radius;
                }
                else
                { 
                    perimetr = Math.PI * dimen.Diameter;
                    squere = Math.PI * dimen.Diameter * dimen.Diameter / 4;
                }

            }
            else if (figure == FigureEnum.Rectangle)
            {
                if (dimen.FirstSide > 0 && dimen.SecondSide > 0)
                {
                    perimetr = 2 * (dimen.FirstSide + dimen.SecondSide);
                    squere = dimen.FirstSide * dimen.SecondSide;
                }
                else if (dimen.ThirdSide > 0 && dimen.SecondSide > 0)
                {
                    perimetr = 2 * (dimen.ThirdSide + dimen.SecondSide);
                    squere = dimen.ThirdSide * dimen.SecondSide;
                }
                else if (dimen.ThirdSide > 0 && dimen.FirstSide > 0)
                {
                    perimetr = 2 * (dimen.ThirdSide + dimen.FirstSide);
                    squere = dimen.ThirdSide * dimen.FirstSide;
                }
            }
            else
            {
                if (dimen.FirstSide > 0 && dimen.SecondSide > 0 && dimen.ThirdSide > 0)
                {
                    perimetr = dimen.FirstSide + dimen.SecondSide + dimen.ThirdSide;
                    squere = perimetr / 2;
                    squere = Math.Sqrt(squere * (squere - dimen.FirstSide) * (squere - dimen.SecondSide) * (squere - dimen.ThirdSide));
                }
                else if (dimen.FirstSide > 0 && dimen.SecondSide > 0 && dimen.Height > 0)
                {
                    squere = dimen.FirstSide * dimen.Height / 2;
                    
                }
                else if (dimen.FirstSide > 0 && dimen.ThirdSide > 0 && dimen.Height > 0)
                {
                    squere = dimen.FirstSide * dimen.Height / 2;
                }
                else if (dimen.FirstSide > 0 && dimen.Height > 0)
                {
                    squere = dimen.FirstSide * dimen.Height / 2;
                }
            }

            if (parameter == ParameterEnum.Perimeter)
            {
                return (int)perimetr;
            }
            else
            {
                return (int)squere;
            }
        }

        private static bool DoubleParse(string s, out double number)
        {
            number = -1;

            int pos = s.IndexOf('.');
            char ch = '.';
            if (pos == -1)
            {
                pos = s.IndexOf(',');
                ch = ',';
            }

            if (pos == -1)
                return false;
            else
            {
                string[] text = s.Split(ch);
                double num1 = int.Parse(text[0]);
                double num2 = int.Parse(text[1]);
                number = num1 + (num2 / Math.Pow(10, text[1].Length));
                return true;
            }
        }
    }
}
